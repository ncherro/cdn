server {
  server_name  cdn.com;
  root  /Users/ncherro/Projects/www/cdn-test/public;

  location ~ ^/images/watermarked/(.*)$ {
    # just return the image
  }

  location ~ ^/images/(.*)$ {

    # NOTE: adding 'none' to list of valid_referers will allow direct links to
    # the image (putting the actual image url in the browser)
    # adding 'blocked' will allow for referers blocked by firewall

    valid_referers server_names
      valid.com *.valid.com;

    if ($invalid_referer) {

      # NOTE: not sure if we should use 'redirect', 'permanent' or 'last' here
      # 'redirect' = 302 redirect
      # 'permanent' = 301 redirect
      # 'last' = serve the watermarked image without redirect - as if it lives at
      # the /images url

      rewrite ^/images/(.*) /watermarked/$1 redirect;
      return 403;
    }
  }

}
